#ifndef UGASI_H
#define UGASI_H

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include "game.h"

class ugasi : public QGraphicsPixmapItem
{
    game * g;
public:
    ugasi(game * g,QString poruka,QGraphicsItem * parent=0);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
};

#endif
