#ifndef PROJEKTIL_H
#define PROJEKTIL_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>

class projektil: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
protected:
    double maksimalniDomet;
    double prijedeniPut;
    int damage;
    int korak;
public:
    projektil(double maksDom,int dmg,QGraphicsItem * parent=0);
    double getMaksimalniDomet();
    double getPrijedeniPut();
    void setMaksimalniDomet(double rng);
    void setPrijedeniPut(double dist);
    int getDamage();
    void setKorak(int br);
    int getKorak();
public slots:
    virtual void pomicanje();
};

#endif
