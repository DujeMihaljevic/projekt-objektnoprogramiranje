#ifndef GRADNJATKATAPULT_H
#define GRADNJATKATAPULT_H

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include "gradnjaTornjeva.h"

class gradnjaTKatapult : public gradnjaTornjeva
{
public:
    gradnjaTKatapult(game * g,QGraphicsItem * parent=0);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
};

#endif
