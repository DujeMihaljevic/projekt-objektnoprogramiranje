#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QMouseEvent>
#include <QGraphicsPixmapItem>
#include "toranj.h"
#include "igrac.h"
#include "nivo.h"

class game: public QGraphicsView
{
    Q_OBJECT
private:
    QGraphicsScene * scena;
    QGraphicsPixmapItem * kursor;
    toranj * build;
    igrac * player;
    nivo * road;
    QTimer *spawnTimer;
    QTimer *provjeraTimer;
    int enemiesSpawned;
public:
    game(nivo * level);
    toranj * getBuild();
    QGraphicsScene * getScena();
    void setBuild(toranj * t);
    void setCursor(QString filename);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    igrac * getIgrac();
    QGraphicsPixmapItem * getCursor();
    void provjera();
    void timerNeprijatelja();
    ~game();
public slots:
    void stvarajNeprijatelje();
    void provjeriUvjete();

};

#endif
