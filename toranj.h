#ifndef TORANJ_H
#define TORANJ_H

#include <QGraphicsPixmapItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsPolygonItem>
#include <QGraphicsItem>
#include <QPointF>
#include <QObject>
#include <QGraphicsScene>

class toranj:  public QObject , public QGraphicsPixmapItem
{
    Q_OBJECT
protected:
    QGraphicsEllipseItem * domet;
    QPointF metaNapada;
    int brzinaNapada;
    bool meta;
    int snagaNapada;
    int polumjer;
    QGraphicsScene *scena;
    QTimer * timer;
public:
    toranj(QGraphicsScene * scena,QGraphicsItem * parent=0);
    double udaljenostDoMete(QGraphicsItem * mete);
    virtual void pucaj();
    void setBrzinaNapada(int brzina);
    int getBrzinaNapada();
    void setDomet(int xSlika, int ySlika);
    void setSnagaNapada(int s);
    int getSnagaNapada();
    void setPolumjer(int p);
    int getPolumjer();
    QGraphicsScene * getScena();
    QPointF getMetaNapada();
    double udaljenostDoMete2(QPointF tocka);
    virtual ~toranj();
public slots:
    void pronadiMetu();

};

#endif
