#include "projektilKamen.h"
#include "neprijatelj.h"
#include <QPixmap>
#include <QTimer>
#include <qmath.h>

projektilKamen::projektilKamen(double maksDom, int dmg, QGraphicsItem *parent):projektil(maksDom,dmg, parent)
{
    setPixmap(QPixmap(":/slike/kamen.jpg"));
    korak=6;
    QTimer * moveTimer= new QTimer(this);
    connect(moveTimer,SIGNAL(timeout()),this,SLOT(pomicanje()));
    moveTimer->start(5);
}

void projektilKamen::pomicanje()
{
    setPrijedeniPut(getPrijedeniPut()+getKorak());
    double kut= rotation();
    double dy=getKorak() * qSin(qDegreesToRadians(kut));
    double dx=getKorak() * qCos(qDegreesToRadians(kut));
    setPos  (x()+dx,y()+dy);

    if(getPrijedeniPut()>=getMaksimalniDomet()-10)
    {
        QList<QGraphicsItem *> sudar =  this->collidingItems();
        if(sudar.size()>1)
        {
            for(size_t i=0,j=sudar.size();i<j;++i)
            {
                neprijatelj * n= dynamic_cast<neprijatelj *>(sudar[i]);
                if(n)
                {
                    (*n).primiStetu(getDamage());
                }
            }
        }
        delete this;
        return;
    }
}
