#ifndef PROJEKTILSTRIJELA_H
#define PROJEKTILSTRIJELA_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include "projektil.h"

class projektilStrijela: public projektil
{
    Q_OBJECT

public:
    projektilStrijela(double maksDom,int dmg,QGraphicsItem * parent=0);
public slots:
    void pomicanje();
};

#endif
