#include "gradnjaTStrijele.h"
#include "toranjStrijele.h"
#include "gradnjaTornjeva.h"

gradnjaTStrijele::gradnjaTStrijele(game* g,QGraphicsItem *parent):gradnjaTornjeva(g,parent)
{
    this->cijena=50;
    setPixmap(QPixmap(":/slike/strijele.png"));
}

void gradnjaTStrijele::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if(i->getNovac()>=this->cijena)
    {
        if(!g->getBuild())
        {
            toranjStrijele* t=new toranjStrijele(g->scene());
            g->setBuild(t);
            g->setCursor(QString(":/slike/strijele.png"));
            i->potrosiNovac(this->cijena);
        }
    }
}

