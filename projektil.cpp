#include "projektil.h"
#include <QPixmap>
#include <QTimer>
#include <qmath.h>
#include "neprijatelj.h"

projektil::projektil(double maksDom,int dmg,QGraphicsItem * parent):QObject(),QGraphicsPixmapItem(parent)
{
    damage=dmg;
    maksimalniDomet=maksDom;
    prijedeniPut=0;
}

void projektil::pomicanje()
{
    QList<QGraphicsItem *> sudar =  this->collidingItems();
    if(sudar.size()>1)
    {
        for(size_t i=0,j=sudar.size();i<j;++i)
        {
            neprijatelj * n= dynamic_cast<neprijatelj *>(sudar[i]);
            if(n)
            {
                (*n).primiStetu(this->damage);
                delete this;
                return;
            }
        }

    }
    double kut= rotation();
    double dy=korak * qSin(qDegreesToRadians(kut));
    double dx=korak * qCos(qDegreesToRadians(kut));
    setPos  (x()+dx,y()+dy);
    prijedeniPut+=korak;
    if(prijedeniPut>=maksimalniDomet)
    {
        delete this;
    }
}

double projektil::getMaksimalniDomet(){
    return maksimalniDomet;
}

double projektil::getPrijedeniPut(){
    return prijedeniPut;
}

void projektil::setMaksimalniDomet(double rng){
    maksimalniDomet = rng;
}

void projektil::setPrijedeniPut(double dist){
    prijedeniPut = dist;
}

int projektil::getDamage()
{
    return this->damage;
}

void projektil::setKorak(int br)
{
    this->korak=br;
}

int projektil::getKorak()
{
    return this->korak;
}
