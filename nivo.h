#ifndef NIVO_H
#define NIVO_H
#include <QList>
#include <QPointF>
#include <QGraphicsRectItem>

class nivo:  public QGraphicsPixmapItem
{
private:
    QVector<QPointF> trasa;
    QGraphicsPolygonItem * put;
    int brNeprijatelja;
public:
    nivo(QVector<QPointF> t, QVector<QPointF> p , int br);
    QVector<QPointF> getTrasa();
    QGraphicsPolygonItem * getPut();
    int getBrNeprijatelja();
};

#endif
