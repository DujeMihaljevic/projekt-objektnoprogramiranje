#include "neprijatelj.h"
#include <QPixmap>
#include <QTimer>
#include <qmath.h>

neprijatelj::neprijatelj(QVector<QPointF> t,QGraphicsPixmapItem *parent)
{
    QVector<QPointF>::iterator it=t.begin();
    for(;it!=t.end();++it)
    {
        this->tocke<<*it;
    }
    odrediste=tocke[0];
    indeksTocke=0;
}

void neprijatelj::rotirajPremaTocki(QPointF tocka)
{
    QLineF ln(pos(),tocka);
    setRotation(-1*ln.angle());
}

void neprijatelj::primiStetu(int dmg)
{
    this->hp-=dmg;
}

void neprijatelj::izracunajCentar(QPointF tocka)
{
    QLineF ln(pos(),tocka);
    centar.setX(pos().x()+pixmap().width()/2* qSin(qDegreesToRadians(ln.angle())));
    centar.setY(pos().y()+pixmap().height()/2* qCos(qDegreesToRadians(ln.angle())));
}

QPointF neprijatelj::getCentar()
{
    return centar;
}

QPointF neprijatelj::getPocetak()
{
    return this->tocke[0];
}

void neprijatelj::idiNaprijed()
{
    if(hp<=0)
    {
        skiniMuHP->potrosiNovac(-50);
        skiniMuHP->incUbijeni();
        delete this;
        return;
    }
    QLineF ln(pos(),odrediste);
    if (ln.length()<=4)
    {
        ++indeksTocke;
        if(indeksTocke>=tocke.size())
        {
            skiniMuHP->oduzmiZivot(vrijednost);
            skiniMuHP->incUbijeni();
            delete this;
            return;  
        }
        odrediste = tocke[indeksTocke];
        rotirajPremaTocki(odrediste);
    }
    double kut= rotation();
    double dy=korak * qSin(qDegreesToRadians(kut));
    double dx=korak * qCos(qDegreesToRadians(kut));
    setPos  (x()+dx,y()+dy);
    //izracunajCentar(odrediste);
    centar.setX(pos().x()+pixmap().width()/2* qSin(qDegreesToRadians(ln.angle())));
    centar.setY(pos().y()+pixmap().height()/2* qCos(qDegreesToRadians(ln.angle())));

}
