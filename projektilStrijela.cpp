#include "projektilStrijela.h"
#include "neprijatelj.h"
#include <QPixmap>
#include <QTimer>
#include <qmath.h>

projektilStrijela::projektilStrijela(double maksDom, int dmg, QGraphicsItem *parent):projektil(maksDom,dmg, parent)
{
    setPixmap(QPixmap(":/slike/metak.png"));
    this->setTransformOriginPoint(this->x()/2,this->y()/2);
    this->setRotation(180);
    korak=10;
    QTimer * moveTimer= new QTimer(this);
    connect(moveTimer,SIGNAL(timeout()),this,SLOT(pomicanje()));
    moveTimer->start(5);
}

void projektilStrijela::pomicanje()
{
    QList<QGraphicsItem *> sudar =  this->collidingItems();
    if(sudar.size()>1)
    {
        for(size_t i=0,j=sudar.size();i<j;++i)
        {
            neprijatelj * n= dynamic_cast<neprijatelj *>(sudar[i]);
            if(n)
            {
                (*n).primiStetu(getDamage());
                delete this;
                return;
            }
        }
    }
    double kut= rotation();
    double dy=getKorak() * qSin(qDegreesToRadians(kut));
    double dx=getKorak() * qCos(qDegreesToRadians(kut));
    setPos  (x()+dx,y()+dy);
    prijedeniPut+=korak;
    if(getPrijedeniPut()>=getMaksimalniDomet())
    {
        delete this;
    }
}
