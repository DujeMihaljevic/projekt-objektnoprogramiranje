#ifndef NEPRIJATELJ_H
#define NEPRIJATELJ_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QList>
#include <QPointF>
#include "igrac.h"

class neprijatelj: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
protected:
    int vrijednost;
    QPointF centar;
    QList<QPointF> tocke;
    QPointF odrediste;
    int indeksTocke;
    int hp;
    int korak;
    igrac * skiniMuHP;
public:
    neprijatelj(QVector<QPointF> t,QGraphicsPixmapItem*parent=0);
    void rotirajPremaTocki(QPointF p);
    void primiStetu(int dmg);
    void izracunajCentar(QPointF tocka);
    QPointF getCentar();
    QPointF getPocetak();

public slots:
    void idiNaprijed();
};

#endif
