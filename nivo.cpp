#include "nivo.h"
#include <QPolygonF>
#include <QVector>
#include <QPointF>
#include <QTimer>

nivo::nivo(QVector<QPointF> t, QVector<QPointF> p, int br)
{
    brNeprijatelja=br;
    QPolygonF polygon(p);
    this->put=new QGraphicsPolygonItem (polygon);
    QVector<QPointF>::iterator it=t.begin();
    for(;it!=t.end();++it)
    {
        this->trasa<<*it;
    }
}

QVector<QPointF> nivo::getTrasa()
{
    return this->trasa;
}

QGraphicsPolygonItem *nivo::getPut()
{
    return this->put;
}

int nivo::getBrNeprijatelja()
{
    return brNeprijatelja;
}



