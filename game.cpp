#include "game.h"
#include "toranj.h"
#include "neprijatelj.h"
#include "gradnjaTStrijele.h"
#include "gradnjaTKatapult.h"
#include "igrac.h"
#include "neprijateljBasic.h"
#include <QGraphicsItem>
#include <QGraphicsScene>
#include "QFont"
#include <QGraphicsTextItem>
#include <QTimer>
#include <iostream>
#include "ugasi.h"

game::game(nivo * level):QGraphicsView()
{
    scena=new QGraphicsScene(this);
    scena->setSceneRect(0,0,1000,800);
    setScene(scena);
    setMouseTracking(true);
    kursor=nullptr;
    build = nullptr;
    //spawn
    spawnTimer=new QTimer(this);
    enemiesSpawned=0;
    provjeraTimer=new QTimer(this);
    road=level;
    provjera();
    scena->addItem(road->getPut());
    setFixedSize(1000,800);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    player =new igrac();
    player->setPos(x()+870,y()+10);
    scena->addItem(player);
    timerNeprijatelja();
    //menu
    gradnjaTStrijele * icon1=new gradnjaTStrijele(this);
    gradnjaTKatapult * icon2=new gradnjaTKatapult(this);
    icon2->setPos(x(),y()+105);
    scena->addItem(icon1);
    scena->addItem(icon2);
    //cijene
    QGraphicsTextItem *cijene=new QGraphicsTextItem();
    cijene->setPlainText(QString("Cijene: ")+"\n"+ QString::number(50)+"$\n\n\n\n"+ QString::number(80)+"$");
    cijene->setDefaultTextColor(Qt::darkGreen);
    cijene->setFont(QFont("times",12));
    cijene->setPos(100,5);
    scena->addItem(cijene);
}

toranj* game::getBuild()
{
    return this->build;
}

QGraphicsScene *game::getScena()
{
    return this->scena;
}

void game::setBuild(toranj * t)
{
    this->build=t;
}

void game::setCursor(QString filename)
{
    if(kursor)
    {
        scena->removeItem(kursor);
    }
    kursor = new QGraphicsPixmapItem();
    kursor->setPixmap(QPixmap(filename));
    scena->addItem(kursor);
}

void game::mouseMoveEvent(QMouseEvent *event)
{
    if(kursor)
    {
        kursor->setPos(event->pos());
    }
}

igrac *game::getIgrac()
{
    return player;
}

void game::mousePressEvent(QMouseEvent *event)
{
    if(build)
    {
        QList<QGraphicsItem *> items = kursor->collidingItems();
        for (size_t i = 0, n = items.size(); i < n; i++)
        {
            if (dynamic_cast<toranj*>(items[i])|| dynamic_cast<QGraphicsPolygonItem * >(items[i]))
            {
                return;
            }
        }
        scena->addItem(build);
        build->setPos(event->pos());
        delete kursor;
        kursor=nullptr;
        build=nullptr;
    }
    else
    {
        QGraphicsView::mousePressEvent(event);
    }
}

QGraphicsPixmapItem *game::getCursor()
{
    return kursor;
}

void game::timerNeprijatelja()
{
    connect(spawnTimer,SIGNAL(timeout()),this,SLOT(stvarajNeprijatelje()));
    spawnTimer->start(1000);
}

game::~game()
{
    delete player;
}

void game::stvarajNeprijatelje()
{
    neprijateljBasic * zlocko=new neprijateljBasic(road->getTrasa(),player);
    zlocko->setPos(zlocko->getPocetak());
    scena->addItem(zlocko);
    ++enemiesSpawned;
    if(enemiesSpawned>=road->getBrNeprijatelja())
        spawnTimer->disconnect();
}
void game::provjera()
{
    connect(provjeraTimer,SIGNAL(timeout()),this,SLOT(provjeriUvjete()));
    provjeraTimer->start(5000);
}

void game::provjeriUvjete()
{
    if(player->getZivot()<=0)
    {
        ugasi * u=new ugasi(this,"PORAZ");
        u->setPos(500,600);
        scene()->addItem(u);
    }
    else if(player->getUbijeni()==road->getBrNeprijatelja())
    {
        ugasi * u=new ugasi(this,"POBJEDA");
        u->setPos(500,600);
        scene()->addItem(u);
    }

}


