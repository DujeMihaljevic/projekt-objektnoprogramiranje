#ifndef GRADNJATORNJEVA_H
#define GRADNJATORNJEVA_H

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include "game.h"
#include "igrac.h"

class gradnjaTornjeva : public QGraphicsPixmapItem
{
protected:
    game * g;
    igrac * i;
    int cijena;
public:
    gradnjaTornjeva(game * g,QGraphicsItem * parent=0);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent * event)=0;
    virtual ~gradnjaTornjeva();
};

#endif
