#ifndef NEPRIJATELJBASIC_H
#define NEPRIJATELJBASIC_H

#include "neprijatelj.h"

class neprijateljBasic: public neprijatelj
{
    Q_OBJECT
public:
    neprijateljBasic(QVector<QPointF> tocke,igrac * player,QGraphicsPixmapItem*parent=0);
public slots:
    void idiNaprijed();
};

#endif
