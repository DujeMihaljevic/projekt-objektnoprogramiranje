#include "ugasi.h"

ugasi::ugasi(game *g,QString poruka,QGraphicsItem *parent)
{
    this->g=g;
    setPixmap(QPixmap(":/slike/ugasi.png"));
    QGraphicsTextItem *obavijest=new QGraphicsTextItem();
    obavijest->setPlainText(QString(poruka));
    obavijest->setDefaultTextColor(Qt::darkGreen);
    obavijest->setFont(QFont("times",50));
    obavijest->setPos(300,300);
    g->getScena()->addItem(obavijest);
}

void ugasi::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    g->~game();
}


