#include <QApplication>
#include "game.h"
#include "nivo.h"
#include "mainmenu.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    mainMenu mm;
    mm.show();

    return a.exec();
}
