#include "toranj.h"
#include "game.h"
#include <QPixmap>
#include <QVector>
#include <QPointF>
#include <QPolygonF>
#include <QObject>
#include <QTimer>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsPixmapItem>
#include "toranjStrijele.h"
#include "projektilStrijela.h"

toranjStrijele::toranjStrijele(QGraphicsScene * scena,QGraphicsItem *parent):toranj(scena,parent)
{
    brzinaNapada=1000;
    snagaNapada=15;
    polumjer=180;
    setPixmap(QPixmap(":/slike/strijele.png"));
    timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(pronadiMetu()));
    timer->start(getBrzinaNapada());
    setDomet( this->pixmap().width()/2,this->pixmap().height()/2);
}

void toranjStrijele::pucaj()
{
    QLineF ln(QPointF(x()+this->pixmap().width()/2,y()+this->pixmap().height()/2),getMetaNapada());
    projektilStrijela *metak = new projektilStrijela(getPolumjer(),getSnagaNapada());
    metak->setPos(x()+this->pixmap().width()/2,y()+this->pixmap().height()/2);
    int angle=ln.angle()*(-1);
    metak->setRotation(angle);
    getScena()->addItem(metak);
}

void toranjStrijele::pronadiMetu()
{
    toranj::pronadiMetu();
}

toranjStrijele::~toranjStrijele(){;}

