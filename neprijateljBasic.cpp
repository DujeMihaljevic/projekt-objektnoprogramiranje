#include "neprijateljBasic.h"
#include <QTimer>

neprijateljBasic::neprijateljBasic(QVector<QPointF> tocke,igrac *player, QGraphicsPixmapItem *parent):neprijatelj(tocke,parent)
{
    setPixmap(QPixmap(":/slike/neprijateljBasic2.png"));
    this->setTransformOriginPoint(this->x()/2,this->y()/2);
    skiniMuHP=player;
    vrijednost=3;
    hp=110;
    korak=4;
    hp=100;
    rotirajPremaTocki(odrediste);
    QTimer *timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(idiNaprijed()));
    timer->start(50);
}

void neprijateljBasic::idiNaprijed()
{
    neprijatelj::idiNaprijed();
}

