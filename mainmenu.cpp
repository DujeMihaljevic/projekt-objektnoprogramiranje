#include "mainmenu.h"
#include "ui_mainmenu.h"
#include <QInputDialog>
#include "nivo.h"
#include <QVector>
#include <QFile>
#include "game.h"
#include <QPixmap>

mainMenu::mainMenu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mainMenu)
{
    ui->setupUi(this);
    QPixmap pic(":/slike/level1.png");
    ui->level1->setPixmap(pic);
    QPixmap pic2(":/slike/level2.png");
    ui->level2->setPixmap(pic2);
}

mainMenu::~mainMenu()
{
    delete ui;
}

void mainMenu::on_pushButton_clicked()
{
    int novi=(QInputDialog::getInt(this,"Unos levela","Unesite broj levela"));
    if(novi>0 && novi<=2)
    {
        QVector<QPointF> points1;
        QVector<QPointF> points2;
        if (novi==1)
        {
            points1 << QPoint(500,0) << QPoint(550,0) << QPoint(550,300) << QPoint(300,300) << QPoint(300,500)<< QPoint(1000,500) << QPoint(1000,550)
                    << QPoint(250,550)<< QPoint(250,250) << QPoint(500,250);
            points2 << QPoint(540,0)<< QPoint(540,260) << QPoint(535,270)<< QPoint(530,280)<< QPoint(515,283)<<
                      QPoint(320,285)<< QPoint(305,290)<< QPoint(295,295)<< QPoint(285,490)<< QPoint(298,500)<< QPoint(310,510)<< QPoint(1000,510);
        }
        if (novi==2)
        {
            points1 << QPoint(300,800) << QPoint(300,200) << QPoint(800,200) << QPoint(800,800) << QPoint(750,800)<<  QPoint(750,250) << QPoint(350,250)<< QPoint(350,800);
            points2 << QPoint(310,800)<< QPoint(310,240)  << QPoint(320,230)<< QPoint(335,220)<< QPoint(344,210) <<
                      QPoint(750,210)<< QPoint(760,220)<< QPoint(775,230)<< QPoint(790,240) << QPoint(790,800) ;
        }
        nivo *level=new nivo(points2,points1,10);
        game *g= new game(level);
        g->show();
    }
}
