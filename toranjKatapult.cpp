#include "toranj.h"
#include "game.h"
#include <QPixmap>
#include <QVector>
#include <QPointF>
#include <QPolygonF>
#include <QObject>
#include <QTimer>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsPixmapItem>
#include "toranjKatapult.h"
#include "projektilKamen.h"
#include <qmath.h>

toranjKatapult::toranjKatapult(QGraphicsScene * scena,QGraphicsItem *parent):toranj(scena,parent)
{
    brzinaNapada=1500;
    snagaNapada=20;
    polumjer=250;
    setPixmap(QPixmap(":/slike/katapult.png"));
    timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(pronadiMetu()));
    timer->start(getBrzinaNapada());
    setDomet( this->pixmap().width()/2,this->pixmap().height()/2);
}

void toranjKatapult::pucaj()
{
    QLineF ln(QPointF(x()+this->pixmap().width()/2,y()+this->pixmap().height()/2),metaNapada);
    projektilKamen *metak = new projektilKamen(ln.length(),getSnagaNapada());
    int kut=ln.angle()*(-1);
    metak->setPos(x()+this->pixmap().width()/2 + metak->pixmap().width()/2*qSin(qDegreesToRadians(ln.angle()*(-1))),
                  y()+this->pixmap().height()/2 + metak->pixmap().height()/(-2)*qCos(qDegreesToRadians(ln.angle())));
    metak->setRotation(kut);
    getScena()->addItem(metak);
}

toranjKatapult::~toranjKatapult(){;}

void toranjKatapult::pronadiMetu()
{
    toranj::pronadiMetu();
}

