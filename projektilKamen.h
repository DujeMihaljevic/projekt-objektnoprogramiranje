#ifndef PROJEKTILKAMEN_H
#define PROJEKTILKAMEN_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include "projektil.h"

class projektilKamen: public projektil
{
    Q_OBJECT
public:
    projektilKamen(double maksDom,int dmg,QGraphicsItem * parent=0);
public slots:
    void pomicanje();
};

#endif
