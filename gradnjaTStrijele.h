#ifndef GRADNJATSTRIJELE_H
#define GRADNJATSTRIJELE_H

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include "gradnjaTornjeva.h"

class gradnjaTStrijele : public gradnjaTornjeva
{
public:
    gradnjaTStrijele(game * g,QGraphicsItem * parent=0);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
};

#endif
