#ifndef TORANJKATAPULT_H
#define TORANJKATAPULT_H

#include "toranj.h"
#include <QGraphicsPixmapItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsPolygonItem>
#include <QGraphicsItem>
#include <QPointF>
#include <QObject>
#include <QGraphicsScene>

class toranjKatapult: public toranj
{
    Q_OBJECT
public:
    toranjKatapult(QGraphicsScene * scena,QGraphicsItem * parent=0);
    void pucaj();
    ~toranjKatapult();
public slots:
    void pronadiMetu();
};

#endif
