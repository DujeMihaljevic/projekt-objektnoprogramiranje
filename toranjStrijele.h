#ifndef TORANJSTRIJELE_H
#define TORANJSTRIJELE_H

#include "toranj.h"
#include <QGraphicsPixmapItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsPolygonItem>
#include <QGraphicsItem>
#include <QPointF>
#include <QObject>
#include <QGraphicsScene>

class toranjStrijele: public toranj
{
    Q_OBJECT
public:
    toranjStrijele(QGraphicsScene * scena,QGraphicsItem * parent=0);
    void pucaj();
    ~toranjStrijele();
public slots:
    void pronadiMetu();
};

#endif
