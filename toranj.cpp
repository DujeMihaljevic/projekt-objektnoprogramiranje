#include "toranj.h"
#include "neprijatelj.h"
#include "projektil.h"
#include "game.h"
#include <QVector>
#include <QPointF>
#include <QPolygonF>
#include <QObject>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsPixmapItem>

#include <iostream>

toranj::toranj(QGraphicsScene * scena,QGraphicsItem *parent):QObject(),QGraphicsPixmapItem(parent)
{
    this->meta=false;
    this->scena=scena;
}

double toranj::udaljenostDoMete(QGraphicsItem *mete)
{
    QLineF ln(QPointF(pos().x()+pixmap().width()/2,pos().y()+pixmap().height()/2), QPointF(mete->pos().x(),mete->pos().y()));
    return ln.length();
}

double toranj::udaljenostDoMete2(QPointF tocka)
{
    QLineF ln(QPointF(pos().x()+pixmap().width()/2,pos().y()+pixmap().height()/2), tocka);
    return ln.length();
}

void toranj::pucaj()
{
    projektil *metak = new projektil(this->polumjer,this->snagaNapada);
    metak->setPos(x()+50,y()+50);
    QLineF ln(QPointF(x()+50,y()+50),metaNapada);
    int angle=ln.angle()*(-1);
    metak->setRotation(angle);
    this->scena->addItem(metak);
}

void toranj::setBrzinaNapada(int brzina)
{
    this->brzinaNapada=brzina;
}

int toranj::getBrzinaNapada()
{
    return this->brzinaNapada;
}

void toranj::setDomet(int xSlika, int ySlika)
{
    this->domet =new QGraphicsEllipseItem(polumjer/(-2),polumjer/(-2),polumjer*2,polumjer*2,this);
    QPointF polyCenter(polumjer/2,polumjer/2);
    polyCenter=mapToScene(polyCenter);
    QPointF towerCenter(x()+xSlika,y()+ ySlika);
    QLineF ln(polyCenter,towerCenter);
    this->domet->setPos(x()+ln.dx(),y()+ln.dy());
}

QGraphicsScene *toranj::getScena()
{
    return this->scena;
}

void toranj::setSnagaNapada(int s)
{
    this->snagaNapada=s;
}

int toranj::getSnagaNapada()
{
    return this->snagaNapada;
}

void toranj::setPolumjer(int p)
{
    this->polumjer=p;
}

int toranj::getPolumjer()
{
    return this->polumjer;
}

QPointF toranj::getMetaNapada()
{
    return this->metaNapada;
}

void toranj::pronadiMetu()
{
    QList<QGraphicsItem *> unutarDometa =  this->domet->collidingItems();
    this->meta=false;
    if(unutarDometa.size()==1)
    {
        this->meta=false;
        return;
    }
    double udaljenostObjekta=300;
    QPointF najblizi = QPointF(0,0);
    for(size_t i=0,j=unutarDometa.size();i<j;++i)
    {
        neprijatelj * n= dynamic_cast<neprijatelj *>(unutarDometa[i]);
        if(n)
        {
            double thisDist = udaljenostDoMete2(n->getCentar());
            if(thisDist<udaljenostObjekta)
            {
                udaljenostObjekta=thisDist;
                najblizi = QPointF(n->getCentar());
                this->meta=true;
            }
        }
    }
    if(this->meta)
    {
        metaNapada=najblizi;
        pucaj();
    }
}

toranj::~toranj()
{
    delete this->domet;
}
