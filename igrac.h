#ifndef IGRAC_H
#define IGRAC_H

#include <QGraphicsTextItem>

class igrac: public QGraphicsTextItem
{
    int novac;
    int zivoti;
    int ubijeni;
public:
    igrac(QGraphicsItem *parent=0);
    void potrosiNovac(int cijena);
    void oduzmiZivot(int neprijatelj);
    int getNovac();
    int getZivot();
    void incUbijeni();
    int getUbijeni();
    QGraphicsTextItem getSemafor();
};

#endif
