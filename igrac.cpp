#include "igrac.h"
#include "QFont"
#include <QGraphicsTextItem>

igrac::igrac(QGraphicsItem *parent):  QGraphicsTextItem(parent)
{
    novac=200;
    zivoti=20;
    ubijeni=0;

    setPlainText(QString("Novac: ")+ QString::number(novac)+"\n"+ QString("Zivoti: ")+QString::number(zivoti));
    setDefaultTextColor(Qt::blue);
    setFont(QFont("times",16));
}

void igrac::potrosiNovac(int cijena)
{
    novac-=cijena;
    setPlainText(QString("Novac: ")+ QString::number(novac)+"\n"+ QString("Zivoti: ")+QString::number(zivoti));
}

void igrac::oduzmiZivot(int neprijatelj)
{
    zivoti-=neprijatelj;
    setPlainText(QString("Novac: ")+ QString::number(novac)+"\n"+ QString("Zivoti: ")+QString::number(zivoti));
}

int igrac::getNovac()
{
    return novac;
}

int igrac::getZivot()
{
    return zivoti;
}

void igrac::incUbijeni()
{
    ++ubijeni;
}

int igrac::getUbijeni()
{
    return ubijeni;
}

